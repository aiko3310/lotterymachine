import styled from 'styled-components';
import { Layout, Card } from 'antd';

export const StyledHeader = styled(Layout.Header)`
  background: #f0f2f5;
  text-align: center;
  padding-top: 0.5rem;
`;
export const StyledContent = styled(Layout.Content)`
  padding: 0.5rem;
  h3 {
    margin-right: 1rem;
    margin-bottom: 0;
  }
`;
export const StyledCenterBox = styled.div`
  margin-top: 1rem;
  display: flex;
  align-items: center;
`;

export const StyledCard = styled(Card)`
  margin-top: 1rem;
`;
