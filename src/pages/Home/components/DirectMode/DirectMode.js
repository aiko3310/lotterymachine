import React, { useState } from 'react';
import EditableCell from './EditableCell';
import EditableFormRow from './EditableFormRow';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../../../store/appStore/action';
import { withRouter } from 'react-router-dom';
import * as ROUTERS from '../../../../router';
import { Row, Col, Input, Card, Table, Button } from 'antd';
import {
  StyledContent,
  StyledInputBox,
  StyledInputAwardBox
} from './StyledDirectMode';
const DirectMode = ({ setDierct, history }) => {
  const [title, setTitle] = useState('');
  const [number, setNumber] = useState(1);
  const [dataSource, setDataSource] = useState([]);
  const [count, setCount] = useState(0);
  const [award, setAward] = useState('');
  const [awardNum, setAwardNum] = useState(1);
  const reverseDataSource = [...dataSource];
  reverseDataSource.reverse();
  const components = {
    body: {
      row: EditableFormRow,
      cell: EditableCell
    }
  };
  const columns = [
    {
      title: '獎項',
      dataIndex: 'name',
      editable: true
    },
    {
      title: '數量',
      dataIndex: 'awardNum',
      editable: true
    },
    {
      title: '刪除',
      dataIndex: 'delete',
      render: (_, record) => (
        <Button
          size='small'
          type='danger'
          onClick={() => handleDelete(record.key)}
        >
          刪除
        </Button>
      )
    }
  ];
  const handleSubmit = () => {
    const numberlength = `${number}`.split('').length;
    const numberArr = new Array(number).fill('');
    const finalNumber = numberArr.map((_, i) => {
      const numberItemLength = `${i + 1}`.split('').length;
      if (numberlength >= numberItemLength) {
        const countNumber = new Array(numberlength - numberItemLength)
          .fill(0)
          .join('');
        return `${title}${countNumber}${i + 1}`;
      }
      return `${title}${i + 1}`;
    });
    const awardArr = [];
    const awardNumArr = [];
    dataSource.forEach(datum => {
      awardArr.push(datum.name);
      awardNumArr.push(datum.awardNum);
    });
    setDierct({
      name: numberArr,
      number: finalNumber,
      award: awardArr,
      count: awardNumArr
    });
    history.push(ROUTERS.LOTTERY);
  };
  const handleAdd = () => {
    const newData = {
      key: count,
      name: award,
      awardNum
    };
    setDataSource(data => [...data, newData]);
    setCount(count => (count += 1));
    setAward('');
    setAwardNum(1);
  };
  const handleSave = row => {
    setDataSource(data =>
      data.map(datum => {
        if (datum.key === row.key) {
          return {
            ...datum,
            ...row
          };
        }
        return datum;
      })
    );
  };
  const handleDelete = key => {
    setDataSource(data => data.filter(datum => datum.key !== key));
  };
  const handleInputChange = (e, setFunc) => {
    setFunc(e.target.value);
  };
  const handleInputNumChange = (e, setFunc) => {
    setFunc(Number(e.target.value));
  };
  const renderColums = columns.map(col => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: record => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave: handleSave
      })
    };
  });
  return (
    <StyledContent justify='center'>
      <Row type='flex' justify='center' gutter={8}>
        <Col xs={22} md={12}>
          <Card>
            <StyledInputBox>
              <h3>數量有開頭嗎？</h3>
              <p>ex: A0000~A0050, 輸入 A00</p>
              <Input
                placeholder='A00'
                value={title}
                onChange={e => handleInputChange(e, setTitle)}
              />
            </StyledInputBox>
            <StyledInputBox>
              <h3>設定人數</h3>
              <Input
                type='number'
                value={number}
                onChange={e => handleInputNumChange(e, setNumber)}
                min={1}
              />
            </StyledInputBox>
          </Card>
        </Col>
      </Row>
      <Row type='flex' justify='center' gutter={8}>
        <Col xs={22} md={12}>
          <Card>
            <h3>設定獎項</h3>
            <StyledInputAwardBox>
              <Input
                placeholder='獎項'
                value={award}
                onChange={e => handleInputChange(e, setAward)}
              />
              <Input
                placeholder='數量'
                value={awardNum}
                onChange={e => handleInputNumChange(e, setAwardNum)}
                type='number'
                min={1}
              />
              <Button
                onClick={handleAdd}
                type='primary'
                style={{ marginBottom: 16 }}
                disabled={!award || !awardNum}
              >
                新增獎項
              </Button>
            </StyledInputAwardBox>
            <Table
              bordered
              components={components}
              dataSource={reverseDataSource}
              columns={renderColums}
            />
          </Card>
        </Col>
      </Row>
      <Row type='flex' justify='center' gutter={8}>
        <Col xs={22} md={12}>
          <Button
            onClick={handleSubmit}
            type='primary'
            style={{ marginBottom: 16 }}
            block
            disabled={dataSource.length === 0}
          >
            抽獎囉
          </Button>
        </Col>
      </Row>
    </StyledContent>
  );
};
export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  ),
  withRouter
)(DirectMode);
