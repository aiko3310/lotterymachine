import styled from 'styled-components';
import { Layout, Card } from 'antd';

export const StyledContent = styled(Layout.Content)`
  padding: 0.5rem;
  > div {
    margin-bottom: 1rem;
  }
`;
export const StyledInputBox = styled.div`
  margin-bottom: 0.5rem;
  p,
  h3 {
    margin-bottom: 0;
  }
`;
export const StyledCard = styled(Card)`
  margin-top: 1rem;
`;
export const StyledInputAwardBox = styled.div`
  display: flex;
  input {
    margin-right: 0.5rem;
  }
`;
