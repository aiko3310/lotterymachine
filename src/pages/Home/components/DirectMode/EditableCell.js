import React, { useState } from 'react';
import { Form, Input } from 'antd';
import { EditableContext } from './EditableFormRow';
const EditableCell = ({
  record,
  handleSave,
  children,
  dataIndex,
  title,
  editable,
  props
}) => {
  const [editing, setEditing] = useState(false);
  const toggleEdit = () => {
    setEditing(editState => !editState);
  };
  const save = (e, form) => {
    form.validateFields((error, values) => {
      if (error && error[e.currentTarget.id]) {
        return;
      }
      toggleEdit();
      handleSave({ ...record, ...values });
    });
  };
  const renderCell = form => {
    if (editing) {
      const renderType = () => {
        if (dataIndex === 'awardNum') {
          return (
            <Input
              onPressEnter={e => save(e, form)}
              onBlur={e => save(e, form)}
              type='number'
              min={1}
            />
          );
        }
        return (
          <Input
            onPressEnter={e => save(e, form)}
            onBlur={e => save(e, form)}
          />
        );
      };
      return (
        <Form.Item style={{ margin: 0 }}>
          {form.getFieldDecorator(dataIndex, {
            rules: [
              {
                required: true,
                message: `${title} 必填`
              }
            ],
            initialValue: record[dataIndex]
          })(renderType())}
        </Form.Item>
      );
    }
    return (
      <div
        className='editable-cell-value-wrap'
        style={{ paddingRight: 24 }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  };
  return (
    <td {...props}>
      {editable ? (
        <EditableContext.Consumer>{renderCell}</EditableContext.Consumer>
      ) : (
        children
      )}
    </td>
  );
};
export default EditableCell;
