import React, { createContext } from 'react';
import { Form } from 'antd';
export const EditableContext = createContext(null);

const EditableRow = ({ form, index, ...props }) => {
  return (
    <EditableContext.Provider value={form}>
      <tr {...props} />
    </EditableContext.Provider>
  );
};

const EditableFormRow = Form.create()(EditableRow);

export default EditableFormRow;
