import React, { useState } from 'react';
import example1 from '../../../../assets/img/example1.jpg';
import example2 from '../../../../assets/img/example2.png';
import example3 from '../../../../assets/img/example3.jpg';
import example4 from '../../../../assets/img/example4.jpg';
import CheckModal from '../CheckModal';
import { Row, Col, Card, Typography, Input } from 'antd';
import { StyledContent, StyledCard } from './StyledGoogleSheetsMode';
const { Title, Text } = Typography;

const GoogleSheetsMode = () => {
  const [keyValue, setKeyValue] = useState('');
  const handleInput = e => {
    setKeyValue(e.target.value);
  };
  return (
    <>
      <StyledContent justify='center'>
        <Row type='flex' justify='center' gutter={8}>
          <Col span={6}>
            <Card>
              <Title level={3}>第一步：製作名單</Title>
              <p>格式：</p>
              <p>請分別在第一行寫上 number, name(可不寫), award, count</p>
              <p>裡面分別代表 號碼,名字, 獎品, 獎品數量</p>
              <p>請從第二行開始填值</p>
            </Card>
          </Col>
          <Col span={8}>
            <Card>
              <img src={example1} alt='' />
            </Card>
          </Col>
        </Row>
      </StyledContent>
      <StyledContent justify='center'>
        <Row type='flex' justify='center' gutter={8}>
          <Col span={6}>
            <Card>
              <Title level={3}>第二步：發佈到網路</Title>
              <p>點開檔案，按下發佈到網路</p>
            </Card>
            <StyledCard>
              <img src={example3} alt='' />
            </StyledCard>
          </Col>
          <Col span={8}>
            <Card>
              <img src={example2} alt='' />
            </Card>
          </Col>
        </Row>
      </StyledContent>
      <StyledContent justify='center'>
        <Row type='flex' justify='center' gutter={8}>
          <Col span={14}>
            <Card>
              <Title level={3}>第三步：把網址裡面的 Key 複製下來</Title>
              <p>
                /spreadsheets/d/ <Text strong>KEY</Text> /edit#gid=0
              </p>
              <img src={example4} alt='' />
            </Card>
          </Col>
        </Row>
      </StyledContent>
      <StyledContent justify='center'>
        <Row type='flex' justify='center' gutter={8}>
          <Col span={14}>
            <Card>
              <Title level={3}>第四步：給我</Title>
              <Row gutter={8}>
                <Col span={20}>
                  <Input
                    value={keyValue}
                    onChange={handleInput}
                    placeholder='給我 Key 就好'
                  />
                </Col>
                <Col span={4}>
                  <CheckModal keyValue={keyValue} />
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </StyledContent>
    </>
  );
};

export default GoogleSheetsMode;
