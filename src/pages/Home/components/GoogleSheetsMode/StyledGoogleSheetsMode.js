import styled from 'styled-components';
import { Layout, Card } from 'antd';

export const StyledContent = styled(Layout.Content)`
  padding: 0.5rem;
`;

export const StyledCard = styled(Card)`
  margin-top: 1rem;
`;
