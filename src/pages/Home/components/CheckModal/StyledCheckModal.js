import styled from 'styled-components';

export const StyledH3 = styled.h3`
  margin-bottom: 0;
`;
export const StyledCenterBox = styled.div`
  margin-top: 1rem;
  display: flex;
  align-items: center;
`;
