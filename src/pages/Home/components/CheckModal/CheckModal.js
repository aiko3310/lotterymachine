import React, { useEffect, useCallback, useReducer } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../../../store/appStore/action';
import { withRouter } from 'react-router-dom';
import * as ROUTERS from '../../../../router';
import PublicGoogleSheetsParser from 'public-google-sheets-parser';
import { initialState, reducer } from './reducer';
import { Modal, Button, Spin } from 'antd';

const CheckModal = ({ keyValue, setList, setNumberMode, history }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { isOpen, init, warn, err, reason } = state;
  const cantEmpty = title => {
    dispatch({ type: 'setWarn', reason: `${title} 要有值` });
  };
  const pushData = (data, datum, title, i) => {
    if (datum) {
      data.push(datum);
    } else {
      dispatch({ type: 'setWarn', reason: `${title}第 ${i} 行要有值` });
    }
  };
  const differentQuantity = title => {
    dispatch({ type: 'setWarn', reason: `${title} 數量不一樣` });
  };

  const handleInit = useCallback(async () => {
    if (keyValue && isOpen && !init) {
      try {
        const parser = new PublicGoogleSheetsParser(keyValue);
        parser.parse().then(data => {
          const name = [];
          const number = [];
          const award = [];
          const count = [];
          data.forEach((datum, i) => {
            pushData(number, datum.number, '號碼', i);
            name.push(datum.name || '');
            if (datum.award) award.push(datum.award);
            if (datum.count) count.push(Number(datum.count));
          });
          if (number.length === 0) {
            cantEmpty('號碼');
          } else if (award.length === 0) {
            cantEmpty('獎品');
          } else if (count.length === 0) {
            cantEmpty('獎品數量');
          } else if (award.length !== count.length) {
            differentQuantity('獎品跟獎品總數');
          } else {
            const haveName = name.some(datum => datum);
            setNumberMode(!haveName);
            setList({
              name,
              number,
              award,
              count
            });
            dispatch({ type: 'clearWarn' });
            history.push(ROUTERS.LOTTERY);
          }
          dispatch({ type: 'init' });
        });
      } catch (err) {
        console.error(err);
        dispatch({ type: 'error' });
      }
    }
  }, [history, init, isOpen, keyValue, setList, setNumberMode]);
  useEffect(() => {
    handleInit();
  }, [handleInit]);
  const handleSubmit = () => {
    dispatch({ type: 'submit' });
  };
  const handleClose = () => {
    dispatch({ type: 'close' });
  };
  const handleToLotteryPage = () => {
    history.push(ROUTERS.LOTTERY);
  };
  const renderFooter = () => {
    if (warn || err || !init) {
      return [
        <Button onClick={handleClose} key='back'>
          回上一步
        </Button>
      ];
    } else {
      return [
        <Button onClick={handleClose} key='back'>
          回上一步
        </Button>,
        <Button onClick={handleToLotteryPage} type='primary' key='next'>
          來抽獎囉
        </Button>
      ];
    }
  };
  const renderBody = () => {
    if (init) {
      if (err) {
        return <p>你給錯 Key 了，連不上</p>;
      } else {
        if (warn) {
          return <p>{reason}</p>;
        }
      }
    } else {
      return <Spin size='large' />;
    }
  };
  return (
    <>
      <Button type='primary' onClick={handleSubmit} disabled={!keyValue}>
        送出囉
      </Button>
      <Modal visible={isOpen} onCancel={handleClose} footer={renderFooter()}>
        {renderBody()}
      </Modal>
    </>
  );
};
export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  ),
  withRouter
)(CheckModal);
