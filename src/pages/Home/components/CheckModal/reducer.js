const initialState = {
  isOpen: false,
  init: false,
  err: false,
  warn: false,
  reason: ''
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'setWarn': {
      return {
        ...state,
        warn: true,
        reason: action.reason
      };
    }
    case 'clearWarn': {
      return {
        ...state,
        warn: false,
        reason: ''
      };
    }
    case 'init': {
      return {
        ...state,
        init: true
      };
    }
    case 'error': {
      return {
        ...state,
        init: true,
        err: true
      };
    }
    case 'submit': {
      return {
        ...state,
        init: false,
        err: false,
        isOpen: true
      };
    }
    case 'close': {
      return {
        ...state,
        isOpen: false
      };
    }
    default: {
      return state;
    }
  }
};
export { initialState, reducer };
