import React, { useState } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../store/appStore/action';
import { Layout, Typography, Input, Radio, Form, Row, Col } from 'antd';
import { StyledHeader, StyledContent, StyledCenterBox } from './styledHome';
import GoogleSheetsMode from './components/GoogleSheetsMode';
import DirectMode from './components/DirectMode';
const { Title } = Typography;
const { Group } = Radio;
const { Footer } = Layout;
const { Item } = Form;

const backgroundOptions = ['不需要', '預設', '自訂'];
const slideBackgroundOptions = ['黑底', '自訂'];
const renderBackgroundOptions = (optionArr, title) =>
  optionArr.map((background, i) => (
    <Radio value={i} key={`${title}-${i}`}>
      {background}
    </Radio>
  ));

const handleValidateStatus = backgroundImg => {
  if (backgroundImg) {
    const reg = /\.(png|jpg|gif)$/;
    if (backgroundImg.match(reg)) {
      return {
        validateStatus: 'success',
        help: ''
      };
    }
    return {
      validateStatus: 'error',
      help: '僅支援圖片格式'
    };
  }
  return {
    validateStatus: 'warning',
    help: '要輸入結尾為 .png 或 .jpg 或 .gif 的網址'
  };
};
const Home = ({
  setFeild,
  backgroundImgType,
  backgroundImg,
  slideBackgroundImgType,
  slideBackgroundImg
}) => {
  const [mode, setMode] = useState(1);
  const handleSetMode = e => {
    setMode(Number(e.target.value));
  };
  const handleFeild = e => {
    const { name, value } = e.target;
    setFeild({ name, value });
  };
  const renderMode = () => {
    switch (mode) {
      case 1: {
        return <GoogleSheetsMode />;
      }
      case 2: {
        return <DirectMode />;
      }
      default: {
        return <GoogleSheetsMode />;
      }
    }
  };
  return (
    <Layout>
      <StyledHeader>
        <Title>抽獎機囉</Title>
      </StyledHeader>
      <StyledContent justify='center'>
        <Row type='flex' justify='center' gutter={8}>
          <Col>
            <h3>有標題嗎？</h3>
            <Input placeholder='設定標題' name='title' onChange={handleFeild} />
            <StyledCenterBox>
              <h3>背景圖？</h3>
              <Group
                onChange={handleFeild}
                value={backgroundImgType}
                name='backgroundImgType'
              >
                {renderBackgroundOptions(backgroundOptions, 'background')}
              </Group>
            </StyledCenterBox>
            {backgroundImgType === 2 && (
              <Form>
                <Item
                  label='背景圖網址'
                  validateStatus={
                    handleValidateStatus(backgroundImg).validateStatus
                  }
                  help={handleValidateStatus(backgroundImg).help}
                  hasFeedback
                >
                  <Input
                    placeholder='背景圖網址 '
                    name='backgroundImg'
                    onChange={handleFeild}
                    value={backgroundImg}
                  />
                </Item>
              </Form>
            )}
            <StyledCenterBox>
              <h3>幻燈片背景圖？</h3>
              <Group
                onChange={handleFeild}
                value={slideBackgroundImgType}
                name='slideBackgroundImgType'
              >
                {renderBackgroundOptions(
                  slideBackgroundOptions,
                  'slideBackground'
                )}
              </Group>
            </StyledCenterBox>
            {slideBackgroundImgType === 1 && (
              <Form>
                <Item
                  label='幻燈片背景圖網址'
                  validateStatus={
                    handleValidateStatus(slideBackgroundImg).validateStatus
                  }
                  help={handleValidateStatus(slideBackgroundImg).help}
                  hasFeedback
                >
                  <Input
                    placeholder='幻燈片背景圖網址 '
                    name='slideBackgroundImg'
                    onChange={handleFeild}
                    value={slideBackgroundImg}
                  />
                </Item>
              </Form>
            )}
            <StyledCenterBox>
              <h3>選擇模式</h3>
              <Group onChange={handleSetMode} value={mode} name='repeat'>
                <Radio value={1}>Google 表單</Radio>
                <Radio value={2}>直接生成</Radio>
              </Group>
            </StyledCenterBox>
          </Col>
        </Row>
      </StyledContent>
      {renderMode()}
      <Footer />
    </Layout>
  );
};
export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )
)(Home);
