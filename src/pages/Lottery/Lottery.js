import React, { useEffect, useReducer, useCallback } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../store/appStore/action';
import { withRouter } from 'react-router-dom';
import * as ROUTERS from '../../router';
// import OrigamiAnimate from './components/OrigamiAnimate';
import SlideAnimate from './components/SlideAnimate';
import lotteryBackground from '../../assets/img/background2.png';
import AddPeopleModal from './components/AddPeopleModal';
import AddAwardModal from './components/AddAwardModal';
// import GeneratePdf from './components/GeneratePdf';
import GenerateExcel from './components/GenerateExcel';
import { listColumns, winnerListColumns } from './columns';
import { initialState, reducer } from './reducer';
import {
  message,
  Layout,
  Row,
  Col,
  Table,
  Typography,
  Badge,
  Radio,
  InputNumber,
  Button
} from 'antd';
import {
  StyledBgWhite,
  StyledLayOut,
  StyledHeader,
  StyledContent,
  StyledAwardButton,
  StyledLotteryButton,
  StyledH3,
  StyledRepeat,
  StyledLotteryButtonWrap,
  StyledCenterBox,
  StyledAnnotation,
  StyledCollapse,
  StyledPanel,
  StyledSettingBox
} from './styledLottery';
const { Footer } = Layout;
const { Title } = Typography;
const { Group } = Radio;
const animateOptions = ['不需要', '幻燈片'];
const showNumberOptions = ['不顯示', '顯示'];
const getRandom = x => Math.floor(Math.random() * x);
const getNoRepeatRandom = (count, mayList) => {
  const obj = {};
  const getRandom = length => {
    const num = Math.floor(Math.random() * length);
    if (obj[num]) {
      return getRandom(length);
    } else {
      obj[num] = mayList[num];
    }
  };
  const x = count - mayList.length >= 0;
  if (x) {
    return mayList;
  }
  new Array(count).fill('_').forEach(_ => {
    getRandom(mayList.length);
  });
  return Object.values(obj);
};
const Lottery = ({
  history,
  list,
  awardList,
  title,
  numberMode,
  backgroundImg,
  backgroundImgType
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const {
    newAwardList,
    newList,
    repeat,
    repeatNum,
    winnerList,
    winnerObj,
    awardObj,
    openAnimate,
    animate,
    mayList,
    pageSize,
    showNumber
  } = state;
  // 顯示順序
  const reverseWinnerList = [...winnerList];
  reverseWinnerList.reverse();
  useEffect(() => {
    if (list.length === 0 || awardList.length === 0) {
      message.warning('頁面重置，自動回到主頁面');
      history.push(ROUTERS.HOME);
    }
  }, [awardList.length, history, list.length]);
  useEffect(() => {
    if (list.length > 0) {
      dispatch({ type: 'updateList', list });
    }
  }, [list]);
  useEffect(() => {
    if (awardList.length > 0) {
      dispatch({ type: 'updateNewAwardList', awardList });
    }
  }, [awardList]);
  const background = useCallback(() => {
    switch (backgroundImgType) {
      case 1:
        return `url(${lotteryBackground})`;
      case 2:
        return `url(${backgroundImg})`;
      default:
        return '';
    }
  }, [backgroundImg, backgroundImgType]);
  const checkAwardChecked = () => {
    const checkedAward = newAwardList.find(award => award.checked);
    if (checkedAward) {
      if (checkedAward.count > 0) {
        if (repeat === 1) {
          const qualifications = newList.filter(
            people => people.repeatNum >= 0
          );
          if (qualifications.length > 0) {
            const winnerCount = winnerList.filter(
              winner => winner.award === checkedAward.award
            ).length;
            return winnerCount === newList.length;
          } else {
            return true;
          }
        } else {
          const qualifications = newList.filter(
            people =>
              !winnerList.find(winner => winner.number === people.number)
          );
          return qualifications.length === 0;
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
  };
  const handleLottery = () => {
    if (animate !== 0) {
      dispatch({ type: 'toggleAnimate', openAnimate: true });
    }
    const awardItem = newAwardList.find(award => award.checked);
    const listCount = newList
      .filter(datum => {
        const hasRepeat = winnerList.find(
          winner => datum.awardKey === winner.awardKey
        );
        if (repeat === 1) {
          return datum.repeatNum >= 0 && !datum.award[awardItem.award];
        } else {
          return !hasRepeat;
        }
      })
      .map(datum => datum.awardKey);
    const mayList = listCount.map(datum =>
      newList.find(people => people.awardKey === datum)
    );
    dispatch({ type: 'setMayList', mayList });
    const winnerAwardKey = listCount[getRandom(listCount.length)];
    const winner = newList.find(people => people.awardKey === winnerAwardKey);
    dispatch({ type: 'getWinner', winner, awardItem });
    if (animate === 0) {
      dispatch({ type: 'lotteryEnd' });
    }
  };
  const handleLotteryAll = () => {
    const awardItem = newAwardList.find(award => award.checked);
    const listCount = newList
      .filter(datum => {
        const hasRepeat = winnerList.find(
          winner => datum.awardKey === winner.awardKey
        );
        if (repeat === 1) {
          return datum.repeatNum >= 0 && !datum.award[awardItem.award];
        } else {
          return !hasRepeat;
        }
      })
      .map(datum => datum.awardKey);
    const mayList = listCount.map(datum =>
      newList.find(people => people.awardKey === datum)
    );
    const winners = getNoRepeatRandom(awardItem.count, mayList);
    dispatch({ type: 'lotteryAllEnd', winners, awardItem });
  };
  const handleCheckAward = index => {
    dispatch({ type: 'clickAward', index });
  };
  const handleRepeatNum = value => {
    if (/^[0-9]*$/g.test(value)) {
      if (value === 0) {
        dispatch({ type: 'setRepeat', value: 2 });
      }
      dispatch({ type: 'setRepeatNum', value });
    } else {
      dispatch({ type: 'setRepeatNum', value: 0 });
      dispatch({ type: 'setRepeat', value: 2 });
    }
  };
  const handleCloseAnimate = () => {
    dispatch({ type: 'toggleAnimate', openAnimate: false });
  };
  const handleFeild = e => {
    const { value, name } = e.target;
    dispatch({ type: 'feild', name, value });
  };
  const handleNumInput = (value, name) => {
    if (/^[0-9]*$/g.test(value)) {
      dispatch({
        type: 'feild',
        name,
        value
      });
    } else {
      dispatch({
        type: 'feild',
        name,
        value: 10
      });
    }
  };
  const renderAward = newAwardList.map((datum, i) => (
    <StyledAwardButton
      onClick={() => handleCheckAward(i)}
      key={`award${i}`}
      disabled={datum.count === 0}
      type={datum.checked ? 'primary' : ''}
    >
      {datum.award}
      <Badge
        count={datum.count}
        showZero={true}
        style={{ backgroundColor: '#87d068' }}
      />
    </StyledAwardButton>
  ));
  const renderLotteryText = checkAwardChecked() ? '選擇獎品' : '抽!';
  const renderOptions = (optionArr, title) =>
    optionArr.map((option, i) => (
      <Radio value={i} key={`${title}-${i}`}>
        {option}
      </Radio>
    ));
  const renderAnimate = () => {
    if (openAnimate) {
      // if (animate === 1) {
      //   return (
      //     <OrigamiAnimate
      //       winnerObj={winnerObj}
      //       awardObj={awardObj}
      //       closeAnimate={handleCloseAnimate}
      //     />
      //   );
      // }
      if (animate === 1) {
        return (
          <SlideAnimate
            mayList={mayList}
            winnerObj={winnerObj}
            awardObj={awardObj}
            closeAnimate={handleCloseAnimate}
            handleLottery={handleLottery}
            showNumber={showNumber}
            lotteryEnd={() => dispatch({ type: 'lotteryEnd' })}
          />
        );
      }
    }
  };
  return (
    <StyledLayOut>
      <StyledHeader>
        <Title>{title || '抽獎！'}</Title>
      </StyledHeader>
      <StyledContent thisimg={background()}>
        <Row gutter={8}>
          <Col span={4} offset={1}>
            <StyledBgWhite>
              <Table
                columns={listColumns(numberMode, showNumber)}
                dataSource={newList}
                size='middle'
                bordered
                scroll={{ y: 400 }}
                title={() => '名單'}
                style={{ backgroundColor: 'white' }}
                pagination={{ pageSize: 5 }}
              />
            </StyledBgWhite>
          </Col>
          <Col span={14} offset={1}>
            <Row>
              <StyledCollapse expandIconPosition='right'>
                <StyledPanel header='參數設定'>
                  <StyledSettingBox>
                    <h3>每個人最多可重複抽</h3>
                    <InputNumber
                      min={0}
                      value={repeatNum}
                      onChange={handleRepeatNum}
                    />
                    <h3>次</h3>
                  </StyledSettingBox>
                  <StyledSettingBox>
                    <h3>這個獎可以重複抽嗎?</h3>
                    <Group onChange={handleFeild} value={repeat} name='repeat'>
                      <Radio value={1} disabled={repeatNum === 0}>
                        可以
                      </Radio>
                      <Radio value={2}>不可以</Radio>
                    </Group>
                  </StyledSettingBox>
                  <StyledSettingBox>
                    <h3>需要動畫？</h3>
                    <Group
                      onChange={handleFeild}
                      value={animate}
                      name='animate'
                    >
                      {renderOptions(animateOptions, 'animate')}
                    </Group>
                  </StyledSettingBox>
                  {!numberMode && (
                    <StyledSettingBox>
                      <h3>顯示號碼？</h3>
                      <Group
                        onChange={handleFeild}
                        value={showNumber}
                        name='showNumber'
                      >
                        {renderOptions(showNumberOptions, 'showNumber')}
                      </Group>
                    </StyledSettingBox>
                  )}
                </StyledPanel>
              </StyledCollapse>
            </Row>
            <Row>
              <StyledCenterBox flex>
                <StyledH3>現在要抽的是</StyledH3>
              </StyledCenterBox>
              {renderAward}
            </Row>
          </Col>
          <Col span={4}>
            <AddPeopleModal dispatch={dispatch} numberMode={numberMode} />
            <AddAwardModal dispatch={dispatch} />
            <GenerateExcel winnerList={reverseWinnerList} />
          </Col>
        </Row>
        <Row gutter={8} type='flex' justify='center'>
          <Col span={12}>
            <StyledLotteryButtonWrap>
              <div>
                <StyledLotteryButton
                  onClick={handleLottery}
                  disabled={checkAwardChecked()}
                >
                  {renderLotteryText}
                </StyledLotteryButton>
              </div>
            </StyledLotteryButtonWrap>
            <Row type='flex' justify='space-between' align='middle'>
              <StyledRepeat>
                <h3>每</h3>
                <InputNumber
                  value={pageSize}
                  onChange={num => handleNumInput(num, 'pageSize')}
                  step={5}
                  min={5}
                />
                <h3>筆為一頁</h3>
              </StyledRepeat>
              <div>
                <Button
                  type='primary'
                  disabled={checkAwardChecked() || animate !== 0}
                  onClick={handleLotteryAll}
                >
                  一次抽完這個獎項
                </Button>
                <StyledAnnotation>
                  <span>*僅限不需要動畫</span>
                </StyledAnnotation>
              </div>
            </Row>
            <StyledBgWhite marginTop={true}>
              <Table
                columns={winnerListColumns(numberMode, showNumber)}
                dataSource={reverseWinnerList}
                pagination={{ pageSize }}
              />
            </StyledBgWhite>
          </Col>
        </Row>
      </StyledContent>
      <Footer />
      {renderAnimate()}
    </StyledLayOut>
  );
};
export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  ),
  withRouter
)(Lottery);
