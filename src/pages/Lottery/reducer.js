const initialState = {
  newAwardList: [],
  newList: [],
  repeat: 2,
  repeatNum: 0,
  winnerList: [],
  winnerObj: {},
  awardObj: {},
  openAnimate: false,
  nextStep: false,
  animate: 1,
  newPeople: '',
  mayList: [],
  pageSize: 10,
  showNumber: 1
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'updateList': {
      return {
        ...state,
        newList: action.list.map((datum, i) => ({
          ...datum,
          awardKey: i,
          key: i,
          repeatNum: 0,
          award: {}
        }))
      };
    }
    case 'updateNewAwardList': {
      return {
        ...state,
        newAwardList: action.awardList.map(datum => ({
          ...datum,
          checked: false
        }))
      };
    }
    case 'addPeople': {
      const i = state.newList.length;
      const people = {
        name: action.request.name,
        number: action.request.num,
        awardKey: i,
        key: i,
        repeatNum: state.repeatNum,
        award: {}
      };
      return {
        ...state,
        newList: state.newList.concat([people])
      };
    }
    case 'addAward': {
      return {
        ...state,
        newAwardList: state.newAwardList.concat([
          {
            award: action.request.award,
            count: action.request.count,
            checked: false
          }
        ])
      };
    }
    case 'lotteryStart': {
      return {
        ...state,
        openAnimate: true
      };
    }
    case 'getWinner': {
      return {
        ...state,
        winnerObj: action.winner,
        awardObj: action.awardItem
      };
    }
    case 'lotteryEnd': {
      const { awardObj, winnerObj } = state;
      const winnerList = state.winnerList.concat([
        {
          award: awardObj.award,
          name: winnerObj.name,
          number: winnerObj.number,
          awardKey: winnerObj.awardKey,
          key: winnerObj.number + awardObj.award
        }
      ]);
      const newList = state.newList.map(people => {
        if (people.repeatNum >= 0 && people.awardKey === winnerObj.awardKey) {
          return {
            ...people,
            award: {
              ...people.award,
              [awardObj.award]: true
            },
            repeatNum: people.repeatNum - 1
          };
        } else {
          return people;
        }
      });
      const newAwardList = state.newAwardList.map(award => ({
        ...award,
        count: award.checked ? award.count - 1 : award.count
      }));
      return {
        ...state,
        winnerList,
        newList,
        newAwardList
      };
    }
    case 'lotteryAllEnd': {
      const { winners, awardItem } = action;
      const newWinners = winners.map(winner => ({
        award: awardItem.award,
        name: winner.name,
        number: winner.number,
        awardKey: winner.awardKey,
        key: winner.number + awardItem.award
      }));
      const winnerList = state.winnerList.concat(newWinners);
      const newAwardList = state.newAwardList.map(award => ({
        ...award,
        count: award.checked ? award.count - winners.length : award.count
      }));
      const newList = state.newList.map(people => {
        if (
          people.repeatNum >= 0 &&
          newWinners.find(winner => winner.awardKey === people.awardKey)
        ) {
          return {
            ...people,
            award: {
              ...people.award,
              [awardItem.award]: true
            },
            repeatNum: people.repeatNum - 1
          };
        } else {
          return people;
        }
      });
      return {
        ...state,
        newList,
        winnerList,
        newAwardList
      };
    }
    case 'toggleAnimate': {
      return {
        ...state,
        openAnimate: action.openAnimate
      };
    }
    case 'clickAward': {
      return {
        ...state,
        newAwardList: state.newAwardList.map((award, i) => ({
          ...award,
          checked: i === action.index ? !award.checked : false
        }))
      };
    }
    case 'setRepeatNum': {
      return {
        ...state,
        repeatNum: action.value,
        newList: state.newList.map(list => ({
          ...list,
          repeatNum: list.repeatNum + action.value - state.repeatNum
        }))
      };
    }
    case 'setMayList': {
      return {
        ...state,
        mayList: action.mayList
      };
    }
    case 'feild': {
      return {
        ...state,
        [action.name]: action.value
      };
    }
    default: {
      return state;
    }
  }
};
export { initialState, reducer };
