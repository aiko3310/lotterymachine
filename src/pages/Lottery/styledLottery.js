import styled from 'styled-components';
import { Layout, Button, Collapse } from 'antd';
const { Panel } = Collapse;
export const StyledBgWhite = styled.div`
  background: white;
  margin-top: ${props => (props.marginTop ? '1rem' : '')};
`;
export const StyledHeader = styled(Layout.Header)`
  background: white;
  text-align: center;
  padding-top: 0.5rem;
  background: #f0f2f5;
`;
export const StyledContent = styled(Layout.Content)`
  padding: 0.5rem;
  background-image: ${props => props.thisimg};
  background-size: cover;
`;
export const StyledLayOut = styled(Layout)`
  min-height: 100vh;
`;
export const StyledAwardButton = styled(Button)`
  margin-right: 0.5rem;
  margin-bottom: 0.5rem;
  > span {
    margin-left: 0.3rem;
  }
`;
export const StyledLotteryButtonWrap = styled.div`
  display: flex;
  justify-content: center;
  > div {
    background: linear-gradient(#e3e3e3, #f2f2f2);
    border-radius: 200px;
    box-shadow: inset 0px 1px 3px rgba(0, 0, 0, 0.04);
    padding: 6px;
  }
`;
export const StyledLotteryButton = styled.button`
  border-radius: 240px;
  width: 120px;
  line-height: 120px;
  height: 120px;
  padding: 0px;
  border-width: 4px;
  font-size: 20px;
  text-align: center;
  background: linear-gradient(#ff4d4f, #cf1322);
  color: white;
  cursor: pointer;
  border-color: #a8071a;
  text-shadow: 0 -1px 1px rgba(0, 40, 50, 0.35);
  &:hover {
    background: linear-gradient(#ff4d4f, #f5222d);
  }
  &:active {
    padding: 4px;
    border: none;
    background: #a8071a;
    text-shadow: rgba(255, 10, 10, 0.62) 1px 1px 0px;
    color: #481414;
  }
  &:focus {
    outline: none;
  }
  &:disabled {
    border-color: #bfbfbf;
    background: linear-gradient(#f5f5f5, #d9d9d9);
    color: #8c8c8c;
    cursor: not-allowed;
    text-shadow: none;
  }
`;
export const StyledH3 = styled.h3`
  display: inline-block;
  font-size: 1.5rem;
  margin-bottom: 0;
  margin-right: 1rem;
`;

export const StyledRepeat = styled.div`
  display: inline-flex;
  align-items: center;
  background: rgba(255, 255, 255, 0.5);
  padding: 0.5rem;
  border-radius: 5px;
  margin-bottom: 0.5rem;
  h3{
    font-size: 1.5rem
    margin-right: 1rem;
    margin-bottom: 0;
    &:last-of-type{
      margin-left: 1rem
    }
  }
  p{
    margin-bottom: 0;
    margin-left: 0.5rem;
  }
`;
export const StyledCenterBox = styled.div`
  display: ${props => (props.flex ? 'flex' : 'inline-flex')};
  align-items: center;
  background: rgba(255, 255, 255, 0.5);
  padding: 0.5rem;
  border-radius: 5px;
  margin-bottom: 0.5rem;
`;

export const StyledAnnotation = styled.div`
  background: rgba(255, 255, 255, 0.5);
  padding: 0.5rem;
  border-radius: 5px;
  margin-top: 0.2rem;
`;

export const StyledCollapse = styled(Collapse)`
  margin-bottom: 0.5rem;
`;

export const StyledPanel = styled(Panel)``;

export const StyledSettingBox = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 0.5rem;
  h3 {
    margin-bottom: 0;
    margin-right: 1rem;
    &:nth-of-type(2) {
      margin-left: 1rem;
    }
  }
`;
