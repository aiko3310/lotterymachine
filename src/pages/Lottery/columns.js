const listColumns = (numberMode, showNumber) => {
  const number = [
    {
      title: '號碼',
      dataIndex: 'number',
      key: 'number',
      align: 'right'
    }
  ];
  const name = [
    {
      title: '名字',
      dataIndex: 'name',
      key: 'name',
      align: 'right'
    }
  ];
  if (numberMode) {
    return number;
  } else if (!showNumber) {
    return name;
  }
  return number.concat(name);
};
const winnerListColumns = (numberMode, showNumber) => {
  const award = [
    {
      title: '獎品',
      dataIndex: 'award',
      key: 'award',
      align: 'left'
    }
  ];
  const number = [
    {
      title: '得獎號碼',
      dataIndex: 'number',
      key: 'number',
      align: 'left'
    }
  ];
  const name = [
    {
      title: '得獎者',
      dataIndex: 'name',
      key: 'name',
      align: 'left'
    }
  ];
  if (numberMode) {
    return award.concat(number);
  } else if (!showNumber) {
    return award.concat(name);
  }
  return award.concat(number, name);
};
export { listColumns, winnerListColumns };
