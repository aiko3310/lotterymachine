import React, { useState } from 'react';
import { Modal, Button, Input } from 'antd';
import { StyledH3, StyledContainer } from './StyledAddPeopleModal';
const AddPeopleModal = ({ dispatch, numberMode }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [num, setNum] = useState('');
  const [name, setName] = useState('');
  const disabledRule = () => {
    if (numberMode) {
      return num === '';
    }
    return num === '' || name === '';
  };
  const handleInput = (e, setFunc) => {
    setFunc(e.target.value);
  };
  const handleToggleModal = () => {
    setIsOpen(state => !state);
    setNum('');
    setName('');
  };
  const handleAdd = () => {
    dispatch({
      type: 'addPeople',
      request: {
        num,
        name
      }
    });
    handleToggleModal();
  };
  const renderFooter = [
    <Button onClick={handleToggleModal} key='cancel'>
      取消
    </Button>,
    <Button
      onClick={handleAdd}
      disabled={disabledRule()}
      key='addPeople'
      type='primary'
    >
      新增
    </Button>
  ];
  return (
    <StyledContainer>
      <Button onClick={handleToggleModal}>新增名單</Button>
      <Modal
        visible={isOpen}
        onCancel={handleToggleModal}
        title='新增名單'
        footer={renderFooter}
      >
        <StyledH3>號碼*</StyledH3>
        <Input
          value={num}
          onChange={e => handleInput(e, setNum)}
          placeholder='號碼'
          required
        />
        {!numberMode && (
          <>
            <StyledH3>名字</StyledH3>
            <Input
              value={name}
              onChange={e => handleInput(e, setName)}
              placeholder='名字'
            />
          </>
        )}
      </Modal>
    </StyledContainer>
  );
};

export default AddPeopleModal;
