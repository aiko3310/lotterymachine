import React from 'react';
import { Button } from 'antd';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../../../store/appStore/action';
import ReactExport from 'react-data-export';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
const GenerateExcel = ({ winnerList, numberMode }) => {
  return (
    <>
      <ExcelFile filename='得獎名單' element={<Button>匯出 Excel</Button>}>
        <ExcelSheet data={winnerList} name='得獎名單'>
          <ExcelColumn label='獎品' value='award' />
          <ExcelColumn label='得獎號碼' value='number' />
          {!numberMode && <ExcelColumn label='得獎人' value='name' />}
        </ExcelSheet>
      </ExcelFile>
    </>
  );
};
export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )
)(GenerateExcel);
