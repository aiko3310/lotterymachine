import React, { useState, useEffect } from 'react';
import Carousel from 'nuka-carousel';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../../../store/appStore/action';
import {
  StyledBackground,
  StyledTitle,
  StyledSlide,
  StyledButtonContainer
} from './StyledSlideAnimate';
import { Button } from 'antd';
const getRandom = x => Math.floor(Math.random() * x);
const maxLength = 14;
const winnerIndex = 9;
const SlideAnimate = ({
  mayList,
  winnerObj,
  awardObj,
  closeAnimate,
  slideBackgroundImg,
  slideBackgroundImgType,
  lotteryEnd,
  showNumber,
  handleLottery
}) => {
  const [list, setList] = useState([]);
  const [showBtn, setShowBtn] = useState(false);
  const [slideIndex, setSlideIndex] = useState(-1);
  const [autoplay, setAutoPlay] = useState(true);
  const [slideTime, setSlideTime] = useState(false);
  const [speed, setSpeed] = useState(100);
  const [listDone, setListDone] = useState(false);
  useEffect(() => {
    if (list.length < maxLength) {
      if (mayList.length > maxLength) {
        const addList = new Array(maxLength)
          .fill({})
          .map(_ => getRandom(mayList.length));
        setList(state => state.concat(addList.map(item => mayList[item])));
      } else {
        const discountLength = maxLength - list.length;
        if (discountLength < mayList.length) {
          const addList = new Array(discountLength)
            .fill({})
            .map(_ => getRandom(mayList.length));
          setList(state => state.concat(addList.map(item => mayList[item])));
        } else {
          setList(state => state.concat(mayList));
        }
      }
    }
  }, [list, mayList]);
  useEffect(() => {
    if (list.length === maxLength) {
      const newList = [...list];
      newList.splice(winnerIndex, 0, winnerObj);
      setList(newList);
      setListDone(true);
    }
  }, [list, winnerObj]);
  const handleBeforeSlide = index => {
    if (index === winnerIndex) {
      if (slideTime) {
        setAutoPlay(false);
        setShowBtn(true);
        lotteryEnd();
      } else {
        setSlideTime(true);
      }
    } else {
      setSpeed(speedState => speedState + 8);
    }
    setSlideIndex(index);
  };
  const handleNext = () => {
    setList([]);
    setShowBtn(false);
    setSlideIndex(-1);
    setAutoPlay(true);
    setSlideTime(false);
    setSpeed(100);
    setListDone(false);
    handleLottery();
  };
  const handleClose = () => {
    closeAnimate();
  };
  const renderBtn = () => {
    if (showBtn) {
      if (awardObj.count - 1 > 0) {
        return (
          <>
            <Button type='primary' onClick={handleNext}>
              抽下一個
            </Button>
            <Button type='primary' onClick={handleClose}>
              關閉視窗
            </Button>
          </>
        );
      }
      return (
        <Button type='primary' onClick={handleClose}>
          抽完了，抽別的吧
        </Button>
      );
    }
  };
  const renderList = list.map((people, i) => (
    <StyledSlide backgroundType={slideBackgroundImgType} key={`people-${i}`}>
      {showNumber && <h3>{people.number}</h3>}
      <h3>{people.name}</h3>
    </StyledSlide>
  ));
  const renderCarousel = () => {
    if (listDone) {
      return (
        <Carousel
          withoutControls
          wrapAround
          slidesToShow={3}
          transitionMode='scroll3d'
          cellAlign='center'
          autoplayInterval={10}
          pauseOnHover={false}
          dragging={false}
          speed={speed}
          opacityScale={0.05}
          autoplay={autoplay}
          slideIndex={slideIndex}
          afterSlide={handleBeforeSlide}
        >
          {renderList}
        </Carousel>
      );
    }
  };
  return (
    <StyledBackground background={slideBackgroundImg}>
      <StyledTitle
        backgroundType={slideBackgroundImgType}
        titlePosition={listDone}
        showNumber={showNumber}
      >
        得到 {awardObj.award} 的是
      </StyledTitle>
      {renderCarousel()}
      <StyledButtonContainer>{renderBtn()}</StyledButtonContainer>
    </StyledBackground>
  );
};
export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )
)(SlideAnimate);
