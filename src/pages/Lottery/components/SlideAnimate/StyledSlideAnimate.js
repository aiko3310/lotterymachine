import styled from 'styled-components';
export const StyledBackground = styled.div`
  z-index: 11;
  background: ${props =>
    props.background
      ? `url(${props.background}) center center/cover`
      : 'rgba(0, 0, 0, 0.92)'};
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`;
export const StyledTitle = styled.h3`
  color: white;
  font-size: 2rem;
  text-align: center;
  ${props => {
    if (props.backgroundType === 1) {
      return {
        background: '#ffffff8a',
        padding: '1rem',
        'border-radius': '7px',
        'text-shadow': '0 0px 10px black'
      };
    }
  }};
  ${props => {
    if (!props.titlePosition) {
      return {
        position: 'absolute',
        top: props.showNumber ? '31.9%' : '36%',
        'z-index': 12
      };
    }
  }}
`;
export const StyledSlide = styled.div`
  > h3 {
    color: white;
    font-size: 2rem;
  }
  ${props => {
    if (props.backgroundType === 1) {
      return {
        background: '#ffffff8a',
        padding: '1rem',
        'border-radius': '7px',
        'text-shadow': '0 0px 10px black'
      };
    }
  }}
  text-align: center;
`;

export const StyledButtonContainer = styled.div`
  position: absolute;
  top: 5%;
  right: 5%;
  animation: scale-in-center 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  display: flex;
  flex-direction: column;
  button {
    margin-bottom: 0.5rem;
  }
  @keyframes scale-in-center {
    0% {
      transform: scale(0);
      opacity: 1;
    }
    100% {
      transform: scale(1);
      opacity: 1;
    }
  }
`;
