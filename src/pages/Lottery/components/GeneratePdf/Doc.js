import React from 'react';
import shnormal from '../../../../assets/font/Microsoft-JhengHei.ttf';
import { Page, Document, Font, StyleSheet, Text } from '@react-pdf/renderer';
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  DataTableCell
} from '@david.kucsai/react-pdf-table';
Font.register({
  family: 'shnormal',
  format: 'truetype',
  src: shnormal
});
const styles = StyleSheet.create({
  body: {
    padding: 20
  },
  th: {
    fontFamily: 'shnormal',
    padding: '5px 10px'
  },
  td: {
    fontFamily: 'shnormal',
    padding: '5px 10px',
    wordBreak: 'break-all',
    // whiteSpace: 'pre-wrap',
    width: 200,
    whiteSpace: 'unset!important'
  },
  pageNumber: {
    position: 'absolute',
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'grey'
  }
});
const Doc = ({ winnerList, numberMode }) => {
  const pageArr = new Array(Math.ceil(winnerList.length / 20))
    .fill('_')
    .map((_, i) => i);
  const renderPages = pageArr.map(num => {
    const filterPage = winnerList.filter(
      (_, i) => i >= num * 20 && i < (num + 1) * 20
    );
    return (
      <Page size='A4' style={styles.body} key={`page-${num}`}>
        <Table data={filterPage}>
          <TableHeader>
            <TableCell styles={styles.th}>獎品</TableCell>
            <TableCell styles={styles.th}>得獎號碼</TableCell>
            {!numberMode && <TableCell styles={styles.th}>得獎人</TableCell>}
          </TableHeader>
          <TableBody>
            <DataTableCell getContent={r => r.award} styles={styles.td} />
            <DataTableCell getContent={r => r.number} styles={styles.td} />
            {!numberMode && (
              <DataTableCell getContent={r => r.name} styles={styles.td} />
            )}
          </TableBody>
        </Table>
        <Text
          style={styles.pageNumber}
          render={({ pageNumber, totalPages }) =>
            `${pageNumber} / ${totalPages}`
          }
          fixed
        />
      </Page>
    );
  });
  return <Document>{renderPages}</Document>;
};

export default Doc;
