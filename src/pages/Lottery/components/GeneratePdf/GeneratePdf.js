import React from 'react';
import { Button } from 'antd';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../../../../store/appStore/action';
import Doc from './Doc';
import { PDFDownloadLink } from '@react-pdf/renderer';

const GeneratePdf = ({ winnerList, numberMode }) => {
  return (
    winnerList.length > 0 && (
      <PDFDownloadLink
        document={<Doc winnerList={winnerList} numberMode={numberMode} />}
        fileName='得獎名單.pdf'
      >
        {({ blob, url, loading, error }) => {
          return (
            <Button disabled={loading} loading={loading}>
              產生 PDF
            </Button>
          );
        }}
      </PDFDownloadLink>
    )
  );
};

export default compose(
  connect(
    state => state.app,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )
)(GeneratePdf);
