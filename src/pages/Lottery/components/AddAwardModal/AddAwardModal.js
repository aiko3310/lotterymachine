import React, { useState } from 'react';
import { Modal, Button, Input } from 'antd';
import { StyledH3, StyledContainer } from './StyledAddAwardModal';

const AddAwardModal = ({ dispatch }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [count, setCount] = useState(1);
  const [award, setAward] = useState('');
  const handleAdd = () => {
    dispatch({
      type: 'addAward',
      request: {
        count,
        award
      }
    });
    handleToggleModal();
  };
  const handleInput = (e, setFunc) => {
    setFunc(e.target.value);
  };
  const handleToggleModal = () => {
    setIsOpen(state => !state);
    setCount(1);
    setAward('');
  };
  const renderFooter = [
    <Button onClick={handleToggleModal} key='cancel'>
      取消
    </Button>,
    <Button onClick={handleAdd} disabled={!award} key='addAward' type='primary'>
      新增
    </Button>
  ];
  return (
    <StyledContainer>
      <Button onClick={handleToggleModal}>新增獎項</Button>
      <Modal
        visible={isOpen}
        onCancel={handleToggleModal}
        title='新增獎項'
        footer={renderFooter}
      >
        <StyledH3>獎項 *</StyledH3>
        <Input
          value={award}
          onChange={e => handleInput(e, setAward)}
          placeholder='獎項'
          required
        />
        <StyledH3>數量</StyledH3>
        <Input
          value={count}
          onChange={e => handleInput(e, setCount)}
          type='number'
          placeholder='數量'
          min={1}
        />
      </Modal>
    </StyledContainer>
  );
};

export default AddAwardModal;
