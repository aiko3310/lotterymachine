import styled from 'styled-components';

export const StyledContainer = styled.div`
  margin-bottom: 0.5rem;
`;

export const StyledH3 = styled.h3`
  margin-bottom: 0.5rem;
  margin-top: 1rem;
  &:first-of-type {
    margin-top: 0;
  }
`;
