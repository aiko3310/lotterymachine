import styled from 'styled-components';
import { Button } from 'antd';
export const StyledBackground = styled.div`
  z-index: 11;
  background: rgba(21, 21, 21, 0.84);
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  animation: scale-in-center 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  @keyframes scale-in-center {
    0% {
      transform: scale(0);
      opacity: 1;
    }
    100% {
      transform: scale(1);
      opacity: 1;
    }
  }
`;
export const StyledTitle = styled.h2`
  color: white;
  font-size: 3rem;
  letter-spacing: 20px;
`;
export const StyledContainer = styled.div`
  width: 80%;
  height: 80%;
  position: relative;
  > div {
    background: white;
    position: absolute;
    user-select: none;
    width: 50%;
    height: 50%;
    &:nth-of-type(3) {
      left: 1px;
      display: ${props => (props.firstDone ? 'block' : 'none')};
      box-shadow: inset -1px -2px 18px 0px rgba(0, 0, 0, 0.1);
    }
    &:nth-of-type(4) {
      left: 1px;
      transform: scaleY(-1);
      transform-origin: left bottom;
      display: ${props => (props.firstDone ? 'block' : 'none')};
      box-shadow: inset -1px -2px 18px 0px rgba(0, 0, 0, 0.1);
    }
  }
`;
export const StyledSecondDiv = styled.div`
  box-shadow: ${props =>
    props.boxshadow === 'first'
      ? '-1px -2px 9px 0px rgba(0, 0, 0, 0.1)'
      : 'inset -1px -2px 18px 0px rgba(0, 0, 0, 0.1)'};
  transform: scaleX(${props => props.stepWidth})
    scaleY(${props => props.firstHeight}) !important;
  transform-origin: ${props =>
    props.firstDone ? 'right bottom' : 'left bottom'};
  z-index: 16;
  cursor: ${props => {
    if (props.firstDone) {
      return props.secondDone ? 'default' : 'ew-resize';
    } else {
      return 's-resize';
    }
  }};
`;
export const StyledFirstDiv = styled.div`
  box-shadow: inset -2px 1px 18px 0px rgba(0, 0, 0, 0.1);
  transform: scaleX(${props => props.stepWidth}) scaleY(1) !important;
  transform-origin: right top;
  z-index: 13;
  cursor: ${props => {
    if (props.firstDone) {
      return props.secondDone ? 'default' : 'ew-resize';
    } else {
      return 's-resize';
    }
  }};
`;
export const StyledName = styled.h3`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  font-size: 3rem;
  z-index: 20;
  display: ${props => (props.secondDone ? 'block' : 'none')}
  animation: focus-in-expand 0.8s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  text-align: center;
  > p {
    margin-bottom: 0;
    line-height: 1.3;
  }
  @keyframes focus-in-expand {
    0% {
      letter-spacing: -0.5em;
      filter: blur(12px);
      opacity: 0;
    }
    100% {
      filter: blur(0px);
      opacity: 1;
    }
  }
`;
export const StyledCloseBtn = styled(Button)`
  transform: scaleX(-1);
  margin: 0.5rem 0.5rem;
`;
