import React, { useState, useLayoutEffect, useEffect, useRef } from 'react';
import Draggable from 'react-draggable';
import {
  StyledBackground,
  StyledContainer,
  StyledSecondDiv,
  StyledFirstDiv,
  StyledTitle,
  StyledName,
  StyledCloseBtn
} from './StyledOrigamiAnimate';
const OrigamiAnimate = ({ winnerObj, awardObj, closeAnimate }) => {
  const [containerSize, setContainerSize] = useState({ x: 0, y: 0 });
  const [firstTransform, setFirstTransform] = useState({ x: 0, y: 0 });
  const [firstHeight, setFirstHeight] = useState(0.95);
  const [stepWidth, setStepWidth] = useState(1);
  const [firstDone, setFirstDone] = useState(false);
  const [secondDone, setSecondDone] = useState(false);
  const [secondTransform, setSecondTransform] = useState({ x: 0, y: 0 });
  const [stepClass, setStepClass] = useState('first');
  const containerRef = useRef();
  const RESET_TIMEOUT = 100;
  const getContainerSize = () => {
    if (containerRef.current) {
      setContainerSize({
        x: containerRef.current.offsetWidth,
        y: containerRef.current.offsetHeight
      });
    }
  };
  useLayoutEffect(() => {
    getContainerSize();
  }, [containerRef]);
  useEffect(() => {
    window.addEventListener('resize', () => {
      setTimeout(getContainerSize, RESET_TIMEOUT);
    });
  }, []);

  const hanldeOnDrag = (e, ui) => {
    if (containerSize.y && !firstDone) {
      const halfContainerSizeY = containerSize.y / 2;
      const h = ui.y * 2 - halfContainerSizeY;
      const computed = h / containerSize.y;
      if (computed * -200 === -100) {
        setFirstTransform({ x: ui.x, y: halfContainerSizeY });
        setSecondTransform({ x: ui.x, y: 0 });
        setFirstDone(true);
        setStepClass('second');
        setFirstHeight(-1);
      } else {
        if (computed * 2 >= -0.95) {
          setFirstHeight(computed * -2);
        }
      }
    }
    if (firstDone && !secondDone) {
      const w = (ui.x * 2 - containerSize.x / 2) / containerSize.x;
      const WNum = w.toFixed(2) * -2;
      setStepWidth(WNum);
      setFirstTransform(state => ({ x: ui.x, y: state.y }));
      setSecondTransform({ x: ui.x, y: 0 });
      if (WNum === -1) {
        setSecondDone(true);
        return false;
      }
    }
  };
  return (
    <StyledBackground>
      <StyledTitle>得獎的是</StyledTitle>
      <StyledContainer
        ref={containerRef}
        firstDone={firstDone}
        stepWidth={stepWidth}
      >
        <Draggable
          axis='x'
          onDrag={hanldeOnDrag}
          bounds='parent'
          handle='.second'
          position={secondTransform}
        >
          <StyledFirstDiv
            stepWidth={stepWidth}
            className={stepClass}
            boxshadow={stepClass}
            firstDone={firstDone}
            secondDone={secondDone}
          >
            {secondDone && (
              <StyledCloseBtn type='primary' onClick={closeAnimate}>
                抽下一個
              </StyledCloseBtn>
            )}
          </StyledFirstDiv>
        </Draggable>
        <Draggable
          axis={stepClass === 'first' ? 'y' : 'x'}
          onDrag={hanldeOnDrag}
          bounds='parent'
          handle={`.${stepClass}`}
          position={firstTransform}
        >
          <StyledSecondDiv
            className={stepClass}
            boxshadow={stepClass}
            firstHeight={firstHeight}
            firstDone={firstDone}
            containerSize={containerSize}
            stepWidth={stepWidth}
            secondDone={secondDone}
          />
        </Draggable>
        <div />
        <div />
        <StyledName secondDone={secondDone}>
          <p>{winnerObj.number}</p>
          <p>{winnerObj.name}</p>
          <p>獲得{awardObj.award}</p>
        </StyledName>
      </StyledContainer>
    </StyledBackground>
  );
};

export default OrigamiAnimate;
