import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { createHashHistory } from 'history';
import * as ROUTES from './router';
import home from './pages/Home';
import lottery from './pages/Lottery';
const history = createHashHistory();

const rootRouter = () => (
  <Router basename={process.env.PUBLIC_URL} history={history}>
    <Switch>
      <Route path={ROUTES.LOTTERY} component={lottery} />
      <Route path={ROUTES.HOME} component={home} />
    </Switch>
  </Router>
);
export default rootRouter;
