import * as actionTypes from './actionTypes';
export const actionCreators = {
  setList: listObj => dispatch => {
    dispatch({ type: actionTypes.SET_LIST, listObj });
  },
  setFeild: feild => dispatch => {
    dispatch({ type: actionTypes.SET_FEILD, feild });
  },
  setNumberMode: boolean => dispatch => {
    dispatch({ type: actionTypes.SET_NUMBER_MODE, boolean });
  },
  setDierct: listObj => dispatch => {
    dispatch({ type: actionTypes.SET_NUMBER_MODE, boolean: true });
    dispatch({ type: actionTypes.SET_LIST, listObj });
  }
};
