import * as actionTypes from './actionTypes';
const initialState = {
  list: [],
  awardList: [],
  title: '',
  backgroundImg: '',
  // 0 不需要 1 預設 2 自訂
  backgroundImgType: 1,
  slideBackgroundImg: '',
  // 0 黑底 1 自訂
  slideBackgroundImgType: 0,
  numberMode: false
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_LIST: {
      const { name, number, award, count } = action.listObj;
      const list = name.map((datum, i) => ({ name: datum, number: number[i] }));
      const awardList = award.map((datum, i) => ({
        award: datum,
        count: count[i]
      }));
      return {
        ...state,
        list: list,
        awardList: awardList
      };
    }
    case actionTypes.SET_FEILD: {
      return {
        ...state,
        [action.feild.name]: action.feild.value
      };
    }
    case actionTypes.SET_NUMBER_MODE: {
      return {
        ...state,
        numberMode: action.boolean
      };
    }

    default: {
      return state;
    }
  }
};
export default reducer;
